# backuper/

Used for building the image to backup neo4j and postgres

# management/

Used to build a wrapper around huniteam/huni-management to allow secrets to
be injected

# docker-compose-database.yml

Deploys the database. Must be run before the web app is deployed.

# docker-compose-webapp.yml

Deploys the app, api, crontasks and management.  

# Starting the services.

Once the swarm is running

Clone this repo.

Connect to docker to the Huni managers


```
export DOCKER_TLS_VERIFY="1"
export DOCKER_HOST="tcp://<huni api uri>"
export DOCKER_CERT_PATH="<huni tls certs>"

```

Deploy the database (only if it is not running)

```
docker stack deploy -c docker-compose-database.yml huni
```

Deploy the web app, api, crontask and management

```
docker stack deploy -c docker-compose-webapp.yml huni
```
