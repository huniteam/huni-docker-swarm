#!/usr/bin/env bash

# https://github.com/pmsipilot/docker-compose-viz

docker run --rm -it --name dcv -v $(pwd):/input \
  pmsipilot/docker-compose-viz render -r -m image --force docker-compose-webapp.yml \
  --output-file=docker-compose-webapp.png

docker run --rm -it --name dcv -v $(pwd):/input \
  pmsipilot/docker-compose-viz render -r -m image --force docker-compose-database.yml \
  --output-file=docker-compose-database.png
