#!/bin/sh

for ID in $(docker service ls -f "label=feed" --format {{.ID}})
do 
    docker service update --force -d  $ID
done


exit 0