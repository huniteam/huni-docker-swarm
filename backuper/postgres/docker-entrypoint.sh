#!/bin/bash
set -e

secrets=(
    aws_secret_access_key
)

for secret_name in ${secrets[@]}; do
    env_name=$(echo ${secret_name} | awk '{ print toupper($0) }')

    if [ -f "${!env_name}" ] 
    then
        export $env_name="$(cat ${!env_name})"
    fi
done

exec "$@"