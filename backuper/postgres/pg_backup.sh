#!/bin/bash

now=`date +"%m_%d_%Y"`

export PGPASSWORD=$(cat $HUNI_HARVEST_PGPASSWORD)
export PGDATABASE="$HUNI_HARVEST_PGDATABASE"
export PGHOST="$HUNI_HARVEST_PGHOST"
export PGUSER="$HUNI_HARVEST_PGUSER"

pg_dump -Fc | gzip -c9 | aws s3 cp - s3://$BUCKET/$FOLDER/"$PGDATABASE"-postgres.dump-"$now".gz

export PGPASSWORD=$(cat $HUNI_PGPASSWORD)
export PGDATABASE="$HUNI_PGDATABASE"
export PGHOST="$HUNI_PGHOST"
export PGUSER="$HUNI_PGUSER"

pg_dump -Fc | gzip -c9 | aws s3 cp --storage-class GLACIER - s3://$BUCKET/$FOLDER/"$PGDATABASE"-postgres.dump-"$now".gz