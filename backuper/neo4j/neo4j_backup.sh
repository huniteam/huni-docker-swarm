#!/bin/bash

now=`date +"%m_%d_%Y"`

dir="$HUNI_BACKUP_DIR/$HUNI_NEO4J_BACKUP_NAME-$now"

if [ ! -d "$dir" ]
then 
    mkdir ${dir}
fi

/var/lib/neo4j/bin/neo4j-admin backup \
    --backup-dir="$dir" \
    --from="$HUNI_NEO4J_HOST":"$HUNI_NEO4J_BACKUP_PORT" \
    --name="$HUNI_NEO4J_BACKUP_NAME"


aws s3 cp --recursive --storage-class GLACIER "$HUNI_BACKUP_DIR" s3://"$BUCKET"/"$FOLDER"

rm -rf /backup/*